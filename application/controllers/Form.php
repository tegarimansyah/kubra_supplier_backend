<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends CI_Controller {

	public function __construct($config = 'rest') {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		header("Access-Control-Allow-Headers: *");

		parent::__construct();
		
		$this->load->model(array('authentication', 'data'));

		// $this->load->library('session');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {
		try {
			if ($this->input->server('REQUEST_METHOD') != 'OPTIONS') {
				$request = file_get_contents('php://input');
				$request = json_decode($request, true);

				$token = $this->input->get('token', TRUE);
				$username = $this->input->get('alias', TRUE);

				$auth = $this->authentication->tokenAuth($token, $username);
				if ($auth['verify']) {
					$mode = $request['mode'];

					if ($mode == 'insert')
						$data = $this->data->insertData($request);
					else if ($mode == 'update')
						$data = $this->data->updateData($request);
					else if ($mode == 'getdata')
						$data = $this->data->getFormData($request);

					if (isset($data) && $data && !isset($data['error']))
						$response = array('code' => 200, 'status' => 'success', 'message' => 'Proses ' . $mode . ' data berhasil', 'data' => $data);
					else if (isset($data) && $data) {
						if (isset($data['error']))
							$data = $data['error'];
						
						$response = array('code' => 200, 'status' => 'fail', 'message' => 'Proses pengolahan data gagal', 'data' => $data);
					} else
						$response = array('code' => 200, 'status' => 'fail', 'message' => 'Proses pengolahan data gagal', 'data' => array());

					echo json_encode($response);
				} else {
					$response = array('code' => 200, 'status' => 'fail_token', 'message' => 'Invalid token', 'error' => 'Ooops... Sepertinya akses anda berakhir... Mohon LogIn ulang aplikasi');
					echo json_encode($response);
				}
			} else {
				echo '';
			}
		} catch (Exception $e) {
			$response = array('code' => 200, 'status' => 'fail', 'message' => 'Proses pengolahan data gagal', 'error' => $e->getMessage());
			echo json_encode($response);
		}
	}

	public function formdata() {
		try {
			if ($this->input->server('REQUEST_METHOD') != 'OPTIONS') {
				$token = $this->input->get('token', TRUE);
				$username = $this->input->get('alias', TRUE);

				$auth = $this->authentication->tokenAuth($token, $username);
				if ($auth['verify']) {
					$uploadRelativePath = $this->config->item('custom')['upload']['relative_url'];
					$uploadAbsoluteSubPath = $this->config->item('custom')['upload']['absolute_sub_url'];
					$uploadConfig['allowed_types'] = 'gif|jpg|jpeg|png';
					
					$request = file_get_contents('php://input');
					// $request = json_decode($this->input->post(), true);
					$request = $this->input->post();
					// print_r($_FILES);
					$form = json_decode($request['formjson'], true);
					$mode = $form['mode'];

					$newValue = array();
					foreach ($form['columns'] as $key => $column) {
						if ($column['type'] == 'file') {
							if (isset($_FILES[$column['prop']])) {
								$filePath = $uploadRelativePath . (isset($column['upload_subfolder']) ? $column['upload_subfolder'] : "");
								if (isset($column['alt_value']['name'])) {
									$uploadConfig['upload_path'] = $filePath;
									$uploadConfig['file_name'] = $column['alt_value']['name'];
									$this->upload->initialize($uploadConfig);

									$filePath = $filePath . $column['alt_value']['name'];
								} else {
									$filePath = $filePath . $_FILES[$column['prop']]['name'];
								}
								// echo 'Filepath: ' . $filePath;
								@unlink($filePath);
								
								$upload = $this->upload->do_upload($column['prop']);
								if ($name = $this->upload->data()) {
									// $column['value'] = site_url() . '/upload/' . $name['file_name'];
									array_push($newValue, array('key' => $key, 'value' => site_url() . $uploadAbsoluteSubPath . (isset($column['upload_subfolder']) ? $column['upload_subfolder'] : "") . $name['file_name'] . '?time=' . time()));
								}
								// echo 'Upload(' . $column['prop'] . '): ' . $upload;
								// print_r(array('upload_data' => $this->upload->data()));
							}
						}
					}

					foreach ($newValue as $value) {
						$form['columns'][$value['key']]['value'] = $value['value'];
					}

					// print_r($form['columns']);

					if ($mode == 'insert')
						$data = $this->data->insertData($form);
					else if ($mode == 'update')
						$data = $this->data->updateData($form);

					if (isset($data) && $data && !isset($data['error']))
						$response = array('code' => 200, 'status' => 'success', 'message' => 'Proses ' . $mode . ' data berhasil', 'data' => $data);
					else if (isset($data) && $data) {
						if (isset($data['error']))
							$data = $data['error'];
						
						$response = array('code' => 200, 'status' => 'fail', 'message' => 'Proses pengolahan data gagal', 'data' => $data);
					} else
						$response = array('code' => 200, 'status' => 'fail', 'message' => 'Proses pengolahan data gagal', 'data' => array());

					echo json_encode($response);
				} else {
					$response = array('code' => 200, 'status' => 'fail_token', 'message' => 'Invalid token', 'error' => 'Ooops... Sepertinya akses anda berakhir... Mohon LogIn ulang aplikasi');
					echo json_encode($response);
				}
			} else {
				echo '';
			}
		} catch (Exception $e) {
			$response = array('code' => 200, 'status' => 'fail', 'message' => 'Proses pengolahan data gagal', 'error' => $e->getMessage());
			echo json_encode($response);
		}
	}

	public function columndata() {
		try {
			if ($this->input->server('REQUEST_METHOD') != 'OPTIONS') {
				$request = file_get_contents('php://input');
				$request = json_decode($request, true);

				$token = $this->input->get('token', TRUE);
				$username = $this->input->get('alias', TRUE);

				$auth = $this->authentication->tokenAuth($token, $username);
				if ($auth['verify']) {
					$data = $this->data->getColumnData($request);

					if (isset($data) && $data && !isset($data['error']))
						$response = array('code' => 200, 'status' => 'success', 'message' => 'Pengambilan data berhasil', 'data' => $data);
					else if (isset($data) && $data) {
						if (isset($data['error']))
							$data = $data['error'];
						
						$response = array('code' => 200, 'status' => 'fail', 'message' => 'Pengambilan data gagal', 'data' => $data);
					} else
						$response = array('code' => 200, 'status' => 'fail', 'message' => 'Pengambilan data gagal', 'data' => array());

					echo json_encode($response);
				} else {
					$response = array('code' => 200, 'status' => 'fail_token', 'message' => 'Invalid token', 'error' => 'Ooops... Sepertinya akses anda berakhir... Mohon LogIn ulang aplikasi');
					echo json_encode($response);
				}
			} else {
				echo '';
			}
		} catch (Exception $e) {
			$response = array('code' => 200, 'status' => 'fail', 'message' => 'Pengambilan data gagal', 'error' => $e->getMessage());
			echo json_encode($response);
		}
	}

	public function test() {
		// print_r($this->config->item('data'));

		// $this->config->set_item('data', 'Test');

		// echo $this->config->item('data');

		// print_r($this->data->test());

		// echo 'Session ID: ' . session_id();
		// print_r($this->session->userdata());
		// print_r($_SESSION);
		
		// $newdata = array(
		// 	'username'  => 'johndoe',
		// 	'email'     => 'johndoe@some-site.com',
		// 	'logged_in' => TRUE
		// );
		
		// $this->session->set_userdata('data', $newdata);

		// print_r($this->session->userdata());
		// print_r($_SESSION);
		
		if (file_exists('data.json')) {
			$read = json_decode(file_get_contents('data.json'), true);

			print_r($read);
		}

		if (isset($read)) {
			$json = $read;
			$json['addition'] = 'addition';
			$json = json_encode($json);
		} else {
			$json = json_encode(array(
				'test' => 'user'
			));
		}
		
		file_put_contents("data.json", $json);
	}
}
