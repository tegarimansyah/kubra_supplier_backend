<?php
class Data extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->database();

		// $this->load->library('session');
	}

	public function insertData($request) {
		$cols = "";
		if (isset($request['columns']) && isset($request['schema_name']) && isset($request['table_name'])) {
			$param = array();
			foreach ($request['columns'] as $column) {
				// print_r($column);
				if (isset($column['value']))
					$param[$column['prop']] = $column['value'];
				else
					$param[$column['prop']] = null;
			}

			if ($this->db->insert(($request['schema_name'] . "." . $request['table_name']), $param)) {
				// try {
				// 	$rows = $this->db->insert_id();
				// } catch (Exception $e) {
				// }

				$affected = $this->db->affected_rows();
			} else {
				$error = $this->db->error();
				if (isset($error['message']) && $error['message'] != '') {
					throw new Exception($error['message']);
				}
			}

			// $data = array("data" => $rows, "total_data" => $total_data, "total_page" => $total_page, "request" => $param);
			$data = array("total_data" => $affected);

			return $data;
		} else {
			return array();
		}
	}

	public function updateData($request) {
		$cols = "";
		if (isset($request['columns']) && isset($request['schema_name']) && isset($request['table_name']) && isset($request['primary'])) {
			$param = array();
			foreach ($request['columns'] as $column) {
				// print_r($column);
				if (isset($column['value']))
					$param[$column['prop']] = $column['value'];
				else
					$param[$column['prop']] = null;
			}

			$primary = array();
			foreach ($request['primary'] as $column) {
				// print_r($column);
				$column['prop'] = escape_columnlist($column['prop']);
				$primary[$column['prop']] = $column['value'];
			}

			if ($this->db->update(($request['schema_name'] . "." . $request['table_name']), $param, $primary)) {
				$affected = $this->db->affected_rows();
			} else {
				$error = $this->db->error();
				if (isset($error['message']) && $error['message'] != '') {
					throw new Exception($error['message']);
				}
			}

			// $data = array("data" => $rows, "total_data" => $total_data, "total_page" => $total_page, "request" => $param);
			$data = array("total_data" => $affected);

			return $data;
		} else {
			return array();
		}
	}
	
	public function getFormData($request){
		$cols = "*";
		if (isset($request['columns']) && isset($request['schema_name']) && isset($request['table_name'])) {
			$cols = "";
			$param = array();
			foreach ($request['columns'] as $column) {
				if (!empty($cols)) {
					$cols = $cols . ", ";
				}

				$cols = $cols . escape_columnlist($column['prop'], $column['type']);
			}

			$cond = '';
			foreach ($request['primary'] as $column) {
				$column['prop'] = escape_columnlist($column['prop']);
				$cond = $cond . " and " . $column['prop'] . " = ? ";
				array_push($param, $column['value']);
			}

			$sql = " SELECT " . $cols . " FROM " . $request['schema_name'] . "." . $request['table_name'] . " where true " . $cond . " ";
			// echo "Query: " . $sql;
			// echo "Param: ";
			// print_r($param);
			$rows = $this->db->query($sql, $param);
			if ($rows) {
				$rows = $rows->result_array();
			} else {
				$rows = array();
			}

			$error = $this->db->error();
			if (isset($error['message']) && $error['message'] != '') {
				throw new Exception($error['message']);
			}

			$data = array("data" => $rows);

			return $data;
		} else {
			return array();
		}
    }
	
	public function getColumnData($request){
		$cols = "*";
		if (isset($request['columns']) && isset($request['schema_name']) && isset($request['table_name'])) {
			$cols = "";
			$param = array();
			$cond = '';
			foreach ($request['columns'] as $column) {
				if (!empty($cols)) {
					$cols = $cols . ", ";
				}

				$column['prop'] = escape_columnlist($column['prop']);
				$cols = $cols . $column['prop'];

				if ($column['search'] && isset($request['value'])) {
					$cond = $cond . " and text(" . $column['prop'] . ") like ? ";
					array_push($param, "%" . $request['value'] . "%");
				}
			}
			if (isset($request['condition'])) {
				$cond = $cond . " " . $request['condition']['prop'] . " ";
				array_push($param, $request['condition']['value']);
			}
			if (isset($request['limit'])) {
				$cond = $cond . " limit ? ";
				array_push($param, $request['limit']);
			}

			$sql = " SELECT " . $cols . " FROM " . $request['schema_name'] . "." . $request['table_name'] . " where true " . $cond . " ";
			$rows = $this->db->query($sql, $param);
			$fields = null;
			if ($rows) {
				$fields = $rows->field_data();
				$rows = $rows->result_array();
			} else {
				$rows = array();
			}

			$error = $this->db->error();
			if (isset($error['message']) && $error['message'] != '') {
				throw new Exception($error['message']);
			}

			$data = array("data" => $this->parseColumnData($request['columns'], $rows, $fields));

			return $data;
		} else {
			return array();
		}
	}
	
	private function parseColumnData($columns, $rows, $fields) {
		$ret = array();

		foreach ($rows as $row) {
			$data = array();

			$fieldIndex = 0;
			$fieldMapper = array();
			foreach ($row as $key => $temp) {
				$fieldMapper[$key] = $fieldIndex;

				$fieldIndex = $fieldIndex + 1;
			}

			// print_r($fieldMapper);

			foreach ($columns as $column) {
				if ($column['value']) {
					$data['id'] = check_valuetype($row[$column['prop']], $fields[$fieldMapper[$column['prop']]]);
				}
				
				if ($column['label']) {
					$data['name'] = check_valuetype($row[$column['prop']], $fields[$fieldMapper[$column['prop']]]);
				}
			}

			array_push($ret, $data);
		}

		return $ret;
	}
	
	public function getData($request){
		$cols = "*";
		if (isset($request['column']) && isset($request['schema_name']) && isset($request['table_name'])) {
			$cols = "";
			// $search = " and ( false ";
			
			$advancedSearch = false;
			$search = "";
			$param = array();
			// echo 'Advanced search: ' . $request['advanced_search'] . ', ' . count($request['advanced_search']);
			// echo '<br/>';
			if (isset($request['advanced_search']) && count($request['advanced_search']) > 0) {
				foreach ($request['advanced_search'] as $column) {
					if ($search == '') {
						$search = $column['condition'] == 'and' ? ' true ' : ' false ';
					}
					
					$search = $search . $column['query'];
					if ($column['check'] == "like")
						array_push($param, "%" . $column['value'] . "%");
					else
						array_push($param, $column['value']);
				}

				$advancedSearch = true;
			} else {
				$search = " false ";
			}
			foreach ($request['column'] as $column) {
				if (!empty($cols)) {
					$cols = $cols . ", ";
				}

				$column['prop'] = escape_columnlist($column['prop']);
				$colsAlias = isset($column['prop_alias']) ? " as " . $column['prop_alias'] . " " : "";
				$cols = $cols . $column['prop'] . $colsAlias;

				if (!$advancedSearch) {
					$search = $search . " or text(" . $column['prop'] . ") like ? ";
					array_push($param, "%" . $request['search'] . "%");
				}
			}

			$order = "";
			// print_r($request['sort']);
			foreach ($request['sort'] as $sort) {
				if (!empty($order)) {
					$order = $order . ", ";
				} else {
					$order = " order by ";
				}

				$order = $order . " " . $sort['prop'] . " " . $sort['dir'] . " ";
			}

			$alias = isset($request['query_alias']) ? " as " . $request['query_alias'] . " " : "";
			$join = isset($request['query_join']) ? " " . $request['query_join'] . " " : "";
			$tableList = " " . $request['schema_name'] . "." . $request['table_name'] . $alias . $join . " ";

			$sql = " SELECT count(*) as total FROM " . $tableList . " where " . $search . "";
			$count = $this->db->query($sql, $param);
			if ($count) {
				$count = $count->row();
			} else {
				$count = 0;
			}

			array_push($param, $request['limit']);
			array_push($param, ($request['page'] * $request['limit']));

			$sql = " SELECT " . $cols . " FROM " . $tableList . " where " . $search . " " . $order . " limit ? offset ? ";
			$rows = $this->db->query($sql, $param);
			if ($rows) {
				$rows = $rows->result_array();
			} else {
				$rows = array();
			}

			$error = $this->db->error();
			if (isset($error['message']) && $error['message'] != '') {
				throw new Exception($error['message']);
			}

			$total_data = $count->total;
			$total_page = ceil($total_data / $request['limit']);

			// $data = array("data" => $rows, "total_data" => $total_data, "total_page" => $total_page, "request" => $param);
			$data = array("data" => $rows, "total_data" => $total_data, "total_page" => $total_page);

			return $data;
		} else {
			return array();
		}
    }

	public function deleteData($request) {
		$cols = "";
		if (isset($request['schema_name']) && isset($request['table_name']) && isset($request['primary'])) {
			$primary = array();
			foreach ($request['primary'] as $column) {
				// print_r($column);
				$column['prop'] = escape_columnlist($column['prop']);
				$primary[$column['prop']] = $column['value'];
			}

			if ($this->db->delete(($request['schema_name'] . "." . $request['table_name']), $primary)) {
				$affected = $this->db->affected_rows();
			} else {
				$error = $this->db->error();
				if (isset($error['message']) && $error['message'] != '') {
					throw new Exception(replace_error($error['message']));
				}
				// if (isset($error)) {
				// 	throw new Exception($error);
				// }
			}

			// $data = array("data" => $rows, "total_data" => $total_data, "total_page" => $total_page, "request" => $param);
			$data = array("total_data" => $affected);

			return $data;
		} else {
			return array();
		}
	}

	// public function test() {
	// 	return $this->session->userdata();
	// }
}
